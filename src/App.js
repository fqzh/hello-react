import React, { Component } from 'react';

import Header from './components/Header';
import Home from './components/Home';

class App extends Component {
  constructor(){
    super();
    this.state = {
      homeLink: 'Home eeemmm...',
      homeMounted: true
    }
  }
  onGreet = (age) => {
    alert(age);
  }

  onChangeLinkName = (newName) =>{
    this.setState({
      homeLink: newName,
    })
  }

  onChangeHomeMounted=()=>{
    this.setState({
      homeMounted:!this.state.homeMounted
    })
  }

  render() {
    const user = {
      name: 'Tom',
      hobbies: ['sports', 'reading']
    }
    let homeCmp = "";
    if(this.state.homeMounted){
      homeCmp = (<Home name={"Max"} initialAge={21} 
      user={user} greet={this.onGreet}
      changeLink={this.onChangeLinkName}
      initialName={this.state.homeLink}
      />);
    }
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-1 col-xs-offset-11">
            <Header homeLink={this.state.homeLink}/>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-1 col-xs-offset-11">
            <h1>hello!!!</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-1 col-xs-offset-11">
            {homeCmp}
          </div>
        </div>
        <hr/>
        <div className="row">
          <div className="col-xs-1 col-xs-offset-11">
            <button className="btn btn-primary" onClick={this.onChangeHomeMounted}>
            mount home component
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
