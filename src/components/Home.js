import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Home extends Component {
  constructor(props){
    super(props);
    this.state = {
      age: props.initialAge,
      name: props.name,
      status: 0,
      homeLink: "Change Link",
    }
    setTimeout(()=>{
      this.setState({
        status: 1
      })
    },1000)
    console.log("constructor~!")
  }

  onMakeOlder=()=>{
    this.setState({
      age: this.state.age + 3
    })
    console.log(this.state);
    console.log(this);
  }

  handleGreet=()=>{
    this.props.greet(this.state.age);
    console.log('打印props:'+this.props.name);
  }

  onChangeLink=()=>{
    this.props.changeLink(this.state.homeLink);
  }

  onHandleChange=(event)=>{
    this.setState({
      homeLink: event.target.value
    })
  }

  componentWillMount=()=>{
    console.log("componentWillMount...")
  }
  componentDidMount=()=>{
    console.log("componentDidMount...")
  }

  componentWillReceiveProps=()=>{
    console.log("componentWillReceiveProps")
  }
  shouldComponentUpdate=(nextProps,nextState)=>{
    console.log("should Component Update", nextProps, nextState)
    if(nextState.status === 1){
      // return false;
    }
    return true;
  }

  componentWillUpdate=(nextProps,nextState)=>{
    console.log("component will Update", nextProps, nextState)
  }
  componentDidUpdate=(prevProps,prevState)=>{
    console.log("component Did Update",prevProps,prevState )
  }

  componentWillUnmount=()=>{
    console.log("component Will Unmount")
  }


  render() {
    console.log("render...")
    return (
      <div className="container">
      <div className="row">
        <div className="col-xs-1 col-xs-offset-11">
          <div>
            your name is {this.props.name},
            your age is {this.props.initialAge},
            '---------state---------'
            your name is {this.state.name},
            your age is {this.state.age},
            status:{this.state.status}
          </div>
          <div>
            {this.props.children}
          </div>
          <button onClick={this.onMakeOlder} className="btn btn-primary">点我</button>
          <hr />
          <button onClick={this.handleGreet} className="btn btn-primary">
              新按钮
          </button>
          <hr/>
          <button onClick={this.props.greet}
          className="btn btn-primary"
          >Greet</button>
          <hr/>
          <input type="text" value={this.state.homeLink} onChange={this.onHandleChange}/>
          <button onClick={this.onChangeLink}>
            Change Header Link
          </button>
        </div>
      </div>
      </div>
    );
  }
};

Home.propTypes = {
  name:PropTypes.string,
  age:PropTypes.number,
  user:PropTypes.object,
  greet:PropTypes.func,
};